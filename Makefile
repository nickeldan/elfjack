CFLAGS := -std=gnu99 -fdiagnostics-color -Wall -Wextra -Werror -fvisibility=hidden
ifeq ($(debug),yes)
    CFLAGS += -O0 -g -DDEBUG
else
    CFLAGS += -O2 -DNDEBUG
endif

all: _all

EJ_DIR := .
include make.mk

APPS_DIR := apps
include apps/make.mk

TEST_DIR := tests
include tests/make.mk

.PHONY: all _all format install uninstall clean

_all: $(EJ_SHARED_LIBRARY) $(EJ_STATIC_LIBRARY) $(APPS)

format:
	@find . -path ./tests/reap -prune -o -name '*.[hc]' -exec clang-format -i {} \;

install: /usr/local/lib/$(notdir $(EJ_SHARED_LIBRARY)) $(foreach file,$(EJ_HEADER_FILES),/usr/local/include/elfjack/$(notdir $(file)))
	ldconfig

/usr/local/lib/$(notdir $(EJ_SHARED_LIBRARY)): $(EJ_SHARED_LIBRARY)
	cp $< $@

/usr/local/include/elfjack/%.h: include/elfjack/%.h | /usr/local/include/elfjack
	cp $< $@

/usr/local/include/elfjack:
	@mkdir -p $@

uninstall:
	rm -rf /usr/local/include/elfjack
	rm -f /usr/local/lib/$(notdir $(EJ_SHARED_LIBRARY))
	ldconfig

clean: $(CLEAN_TARGETS)
