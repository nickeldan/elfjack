ifndef EJ_MK
EJ_MK :=

EJ_LIB_DIR ?= $(EJ_DIR)
EJ_OBJ_DIR ?= $(EJ_DIR)/src

EJ_SHARED_LIBRARY := $(EJ_LIB_DIR)/libelfjack.so
EJ_STATIC_LIBRARY := $(EJ_LIB_DIR)/libelfjack.a

EJ_SOURCE_FILES := $(wildcard $(EJ_DIR)/src/*.c)
EJ_OBJECT_FILES := $(patsubst $(EJ_DIR)/src/%.c,$(EJ_OBJ_DIR)/%.o,$(EJ_SOURCE_FILES))
EJ_HEADER_FILES := $(wildcard $(EJ_DIR)/include/elfjack/*.h)
EJ_INCLUDE_FLAGS := -I$(EJ_DIR)/include

$(EJ_OBJ_DIR)/%.o: $(EJ_DIR)/src/%.c $(EJ_HEADER_FILES) $(wildcard $(EJ_DIR)/src/*.h) | $(EJ_OBJ_DIR)
	$(CC) $(CFLAGS) $(EJ_INCLUDE_FLAGS) -fpic -ffunction-sections -c $< -o $@

$(EJ_SHARED_LIBRARY): $(EJ_OBJECT_FILES) | $(EJ_LIB_DIR)
	$(CC) $(LDFLAGS) -shared -o $@ $^

$(EJ_STATIC_LIBRARY): $(EJ_OBJECT_FILES) | $(EJ_LIB_DIR)
	$(AR) rcs $@ $^

$(EJ_OBJ_DIR) $(EJ_LIB_DIR):
	@mkdir -p $@

ej_clean:
	@rm -f $(EJ_SHARED_LIBRARY) $(EJ_STATIC_LIBRARY) $(EJ_OBJECT_FILES)

.PHONY: ej_clean
CLEAN_TARGETS += ej_clean

endif
