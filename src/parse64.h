#pragma once

#include <stdbool.h>

#include "internal.h"

int
ejFindLoadAddr64(const struct ejIntHelpers *helpers, const void *pheader, uint32_t phnum,
                 unsigned int *load_addr);

int
ejFindShdrs64(ejElfInfo *info, const void **dynamic_section, uint64_t *num_dynamic_entries,
              const struct ehdrParams *params);

bool
ejIsRelroFull64(const struct ejIntHelpers *helpers, const void *dynamic_section,
                uint64_t num_dynamic_entries);

bool
ejFindSymbol64(const ejElfInfo *info, const char *func_name, uint16_t section_index, ejSymbolValue *value);

ejAddr
ejFindGotEntry64(const ejElfInfo *info, uint64_t symbol_index);
