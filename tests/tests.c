#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <elfjack/elfjack.h>
#include <reap/reap.h>
#include <scrutiny/scrutiny.h>

static const char *lib_path;
static const char *full_path;
static const char *partial_path;

struct full_ctx {
    void *handle;
    unsigned long long lib_start;
};

static unsigned long long
libDummyStart(void)
{
    int err;
    char path[1024];
    reapMapIterator *iterator;
    reapMapResult result;

    scrAssertEq(reapMapIteratorCreate(getpid(), &iterator), REAP_RET_OK);

    while ((err = reapMapIteratorNext(iterator, &result, path, sizeof(path))) == REAP_RET_OK) {
        if (strstr(path, "libdummy.so")) {
            reapMapIteratorDestroy(iterator);
            return result.start;
        }
    }

    if (err != REAP_RET_DONE) {
        scrFail("Failed to walk memory map: %s", reapGetError());
    }
    scrFail("Failed to find libdummy.so in memory map");
}

static void *
openLibrary(void *global_ctx)
{
    struct full_ctx *full_ctx;

    (void)global_ctx;

    full_ctx = malloc(sizeof(*full_ctx));
    scrAssertPtrNe(full_ctx, NULL);

    full_ctx->handle = dlopen(lib_path, RTLD_NOW);
    if (!full_ctx->handle) {
        scrFail("dlopen (%s): %s", lib_path, dlerror());
    }
    full_ctx->lib_start = libDummyStart();

    return full_ctx;
}

static void
cleanup(void *ctx)
{
    struct full_ctx *full_ctx = ctx;

    dlclose(full_ctx->handle);
    free(full_ctx);
}

static pid_t
fakeGetpid(void)
{
    return 0;
}

static void
find_function_start(void)
{
    ejAddr function_start;
    ejElfInfo info;
    struct full_ctx *full_ctx;
    int (*foo_ptr)(void);

    full_ctx = scrGroupCtx();
    if (ejParseElf(lib_path, &info) != EJ_RET_OK) {
        scrFail("Failed to parse ELF: %s", ejGetError());
    }

    function_start = ejFindFunction(&info, "foo");
    scrAssertNe(function_start, EJ_ADDR_NOT_FOUND);
    function_start = ejResolveAddress(&info, function_start, full_ctx->lib_start);
    ejReleaseInfo(&info);

    foo_ptr = (int (*)(void))function_start;
    scrAssertEq(foo_ptr(), 56);
}

static void
find_got_entry(void)
{
    ejAddr got_entry, function_start;
    ejElfInfo info;
    struct full_ctx *full_ctx;
    pid_t (*bar_ptr)(void);

    full_ctx = scrGroupCtx();
    if (ejParseElf(lib_path, &info) != EJ_RET_OK) {
        scrFail("Failed to parse ELF: %s", ejGetError());
    }

    got_entry = ejFindGotEntry(&info, "getpid");
    scrAssertNe(got_entry, EJ_ADDR_NOT_FOUND);
    got_entry = ejResolveAddress(&info, got_entry, full_ctx->lib_start);

    function_start = ejFindFunction(&info, "bar");
    scrAssertNe(function_start, EJ_ADDR_NOT_FOUND);
    function_start = ejResolveAddress(&info, function_start, full_ctx->lib_start);
    ejReleaseInfo(&info);

    bar_ptr = (pid_t(*)(void))function_start;
    scrAssertEq(bar_ptr(), getpid());

    *(void **)got_entry = fakeGetpid;
    scrAssertEq(bar_ptr(), 0);
}

static void
detect_relro(void)
{
    ejElfInfo info;

    if (ejParseElf(full_path, &info) != EJ_RET_OK) {
        scrFail("Failed to parse ELF: %s", ejGetError());
    }

    scrAssertEq(info.visible.relro, 1);
}

static void
partial_relro_ignored(void)
{
    ejElfInfo info;

    if (ejParseElf(partial_path, &info) != EJ_RET_OK) {
        scrFail("Failed to parse ELF: %s", ejGetError());
    }

    scrAssertEq(info.visible.relro, 0);
}

int
main(int argc, char **argv)
{
    scrGroup group;
    scrGroupOptions group_options = {
        .create_fn = openLibrary,
        .cleanup_fn = cleanup,
    };

    if (argc < 4) {
        fprintf(stderr, "Missing paths\n");
        return 1;
    }
    lib_path = argv[1];
    full_path = argv[2];
    partial_path = argv[3];

    group = scrGroupCreate(&group_options);
    scrAddTest(group, "Find function start", find_function_start, NULL);
    scrAddTest(group, "Find GOT entry", find_got_entry, NULL);
    scrAddTest(group, "Detect RELRO", detect_relro, NULL);
    scrAddTest(group, "Partial RELRO ignored", partial_relro_ignored, NULL);

    return scrRun(NULL, NULL);
}
