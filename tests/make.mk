TEST_BINARY := $(TEST_DIR)/tests
LIBDUMMY := $(TEST_DIR)/libdummy.so
FULL_BINARY	:= $(TEST_DIR)/full
PARTIAL_BINARY := $(TEST_DIR)/partial

ifeq ($(MAKECMDGOALS),tests)

REAP_DIR := $(TEST_DIR)/reap
include $(REAP_DIR)/make.mk

$(LIBDUMMY): $(TEST_DIR)/dummy.c
	$(CC) -shared -fPIC -O0 $< -o $@

$(FULL_BINARY): $(TEST_DIR)/hello.c
	$(CC) -Wl,-z,-relro,-z,now $< -o $@

$(PARTIAL_BINARY): $(TEST_DIR)/hello.c
	$(CC) -Wl,-z,-relro,-z,lazy $< -o $@

$(TEST_BINARY): $(TEST_DIR)/tests.c $(EJ_STATIC_LIBRARY) $(REAP_STATIC_LIBRARY)
	$(CC) $(CFLAGS) $(EJ_INCLUDE_FLAGS) $(REAP_INCLUDE_FLAGS) $^ -lscrutiny -o $@

.PHONY:
tests: $(TEST_BINARY) $(LIBDUMMY) $(FULL_BINARY) $(PARTIAL_BINARY)
	@$< $(LIBDUMMY) $(FULL_BINARY) $(PARTIAL_BINARY)

endif

.PHONY:
test_clean:
	@rm -f $(TEST_BINARY) $(LIBDUMMY) $(FULL_BINARY) $(PARTIAL_BINARY)

CLEAN_TARGETS += test_clean
