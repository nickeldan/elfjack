#include <stdio.h>
#include <unistd.h>

int
foo(void)
{
    printf("foo called\n");
    return 56;
}

pid_t
bar(void)
{
    printf("bar called\n");
    return getpid();
}
