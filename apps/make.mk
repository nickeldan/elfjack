APPS := $(patsubst $(APPS_DIR)/%.c,%,$(wildcard $(APPS_DIR)/*.c))

%: $(APPS_DIR)/%.c $(EJ_STATIC_LIBRARY)
	$(CC) $(CFLAGS) $(EJ_INCLUDE_FLAGS) $^ -o $@

.PHONY:
app_clean:
	@rm -f $(APPS)

CLEAN_TARGETS += app_clean
